// State
/*
- PlayerA's score
- PlayerB's score
- Match result
*/

// Behavior
/*
- PlayerA get score
- PlayerB get score
- Reset score
- PlayerA wins the game
- PlayerB wins the game
- Echo score
*/

function TennisGame() {
    this.playerAScore
    this.playerBScore

    this. score=[
        'Love',
        'Fifteen',
        'Thirty',
        'Forty'
        
    ]


    this.start = () => {
        this.playerAScore = 0
        this.playerBScore = 0
    }

    this.echoScore = () => {
        if((this.playerAScore-this.playerBScore===1)&&(this.playerAScore>=3&&this.playerBScore>=3)){
            return 'PlayerA advantage'
        }
        if((this.playerAScore===this.playerBScore)&&(this.playerAScore>=3&&this.playerBScore>=3)){
            return 'Deuce'
        }
        if(this.playerAScore>3){
            return 'PlayerA wins game'
        }
        if(this.playerBScore>3){
            return 'PlayerB wins game'
        }
        if((this.playerAScore-this.playerBScore===2)&&(this.playerAScore>=3&&this.playerBScore>=3)){
            return 'PlayerA wins game'
        }
        return this.score[this.playerAScore]+" - "+this.score[this.playerBScore]
        
    }

    this.playerAGetScore = () => {
        this.playerAScore++
    }

    this.playerBGetScore = () => {
        this.playerBScore++
    }
}

var tennisGame

function startGame() {
    tennisGame = new TennisGame()
    tennisGame.start()
}

function addScorePlayerA(count) {
    for (let i = 0; i < count; i++) {
        tennisGame.playerAGetScore()
    }
}

function addScorePlayerB(count) {
    for (let i = 0; i < count; i++) {
        tennisGame.playerBGetScore()
    }
}

test('"Love - Love" when game started', () => {
    startGame()

    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Love')
})

test('"Fifteen - Love" when A get first score', () => {
    startGame()
    addScorePlayerA(1)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Fifteen - Love')
})

test('"Thirty - Love" when A get double scored', () => {
    startGame()
    addScorePlayerA(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Thirty - Love')
})

test('"Forty - Love" when A get triple score', () => {
    startGame()
    addScorePlayerA(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Forty - Love')
})

test('"PlayerA wins game when A get 4 scores before B"', () => {
    startGame()

    addScorePlayerA(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA wins game')
})

test('"Love - Fifteen" when B get first score', () => {
    startGame()
    addScorePlayerB(1)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Fifteen')
})

test('"Love - Thirty" when B get double scored', () => {
    startGame()
    addScorePlayerB(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Love - Thirty')
})

test('"PlayerB wins game when B get 4 scores before A"', () => {
    startGame()

    addScorePlayerB(4)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerB wins game')
})

test('"PlayerA wins game when A get 4 scores before B"', () => {
    startGame()

    addScorePlayerA(4)
    addScorePlayerB(2)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA wins game')
})

test('"Deuce" when score is 40-40', () => {
    startGame()
    addScorePlayerA(3)
    addScorePlayerB(3)
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('Deuce')
})

test('"PlayerA advantage" when A get score after deuce', () => {
    startGame()
    addScorePlayerA(3)
    addScorePlayerB(3)
    tennisGame.playerAGetScore()
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA advantage')
})
test('"PlayerA wins game" when A get 2 score after deuce', () => {
    startGame()
    addScorePlayerA(3)
    addScorePlayerB(3)
    tennisGame.playerAGetScore()
    tennisGame.playerAGetScore()
    var scoreString = tennisGame.echoScore()

    expect(scoreString).toBe('PlayerA wins game')
})